
MethodNode::MethodNode(void)
{
	name = "";
	body = NULL;
	returnType = NULL;
	isStatic = false;
}


MethodNode::~MethodNode(void)
{
}

string MethodNode::descriptor()
{
	string res = "(";

	for(int i = 0; i < arg.size(); i++)
	{
		res += localTable[arg[i]]->type->toStringForDescriptor();;
	}

	res += ")";
	res += returnType->toStringForDescriptor();
	return res;
}

//Вызывать можно только на третьем проходе, или на втором, если уверены что все поля заполнены
string MethodNode::header()
{
	
	if(res.find_first_of("Classes/") == 0)
	{
		string::iterator it = res.begin();
		res.erase(it, it+8);
	}
	return res;
}

void MethodNode::printTable(FILE* filename)
{
	fprintf(filename, "ТАБЛИЦА ЛОКАЛЬНЫХ ПЕРЕМЕННЫХ:\n");
	fprintf(filename, "№;Тип;Имя;\n");
	
}

string MethodNode::toStringForPrint()
{
	string str;
	string stat;
	if(this->isStatic)
		stat = "true;";
	else
		stat = "false;";

	string param = "";
	for(int i = 0; i < arg.size(); i++)
	{
		param += arg[i];
		if(i != arg.size() - 1)
			param += ", ";
	}
	param += ";";
	sdrhbdzhgrgredgaegaregargregaerg

	str = this->identificator + string(";") + stat + this->returnType->toStringWithArray() + string(";") +
		this->name + string(";") + this->outRef + string(";") + param;
	return str;
}

void MethodNode::writeClassFile(std::vector<char>* classFile, ClassNode* clss)
{
	 //Записываем флаги доступа ACC_PUBLIC (0x0001) и ACC_STATIC (0x0008), если функция Main
		if(isStatic)
			writeU2(9, classFile);
		else
			writeU2(1, classFile);

		// Записываем номер константы типа CONSTANT_Utf8, содержащей имя метода 
		writeU2(clss->findUTF8(name), classFile);

		// Записываем номер константы типа CONSTANT_Utf8, содержащей дескриптор метода
		writeU2(clss->findUTF8(descriptor()), classFile);

		// Записываем количество атрибутов метода
		writeU2(1, classFile);

		// Записываем таблицу атрибутов метода
		writeCodeMethodsToClassFile(classFile, clss);
}

void MethodNode::writeCodeMethodsToClassFile(std::vector<char>* classFile, ClassNode* clss)
{
	writeU2(clss->findUTF8("Code"), classFile);

	int lengthAttrPos = classFile->size(); // позиция в которую вставлять длину атрибута

	// Записываем размер стека операндов для метода
	writeU2(1000, classFile); //просто поставить 1000 или более

	// Записываем количество локальных переменных метода
	writeU2(localTable.size(), classFile);

	int lengthByteCode = classFile->size(); //позиция в которую вставлять длину байт кода

	int withoutByteCode = classFile->size(); //размер без байт кода
		
	generation_stmtList(body, classFile, clss); //здесь вся генерация, внутри проход по узлам

	int sizeByteCode = classFile->size() - lengthByteCode; //размер байткода

	//вставка размера байт кода
	writeU4(sizeByteCode, classFile, lengthByteCode);
	
	// Записываем количество записей в таблице исключений
	writeU2(0, classFile);

	// Записываем количество атрибутов
	writeU2(0, classFile);

	int lengthAttr = classFile->size() - lengthAttrPos;
	//Вставка длины атрибута
	writeU4(lengthAttr, classFile, lengthAttrPos);
}

void MethodNode::generation_stmtList(stmtListStruct* stmtList, std::vector<char>* classFile, ClassNode* clss)
{
	stmtStruct* tmp = NULL;
	if(stmtList != NULL)
	{
		tmp = stmtList->stmtFirst;
	}
	while(tmp != NULL)
	{
		switch(tmp->type)
		{
			case ExprStmt:
				generation_expr(tmp->expr, classFile, clss, false);
				if(tmp->expr->type == New && tmp->expr->newAppeal->type == MethodAppeal)
				{
					classFile->push_back(87);//pop - если выражение только одно в строке, никуда не присваивается
				}
				break;
			case DeclarationStmt:
				break;
			case IfStmt:
				generation_ifStmt(tmp->ifElse, classFile, clss);
				break;
			case WhileStmt:
				if(tmp->whileLoop->type == _while)
					generation_whileStmt(tmp->whileLoop, classFile, clss);
				else
					generation_doWhileStmt(tmp->whileLoop, classFile, clss);
				break;
			case StmtListStmt:
				generation_stmtList(tmp->stmtList, classFile, clss);
				break;
			case ReturnStmt:
				if(tmp->returnExpr == NULL)
					classFile->push_back(177); //пустой return
				else
				{
					generation_expr(tmp->returnExpr, classFile, clss);
					if(tmp->returnExpr->typeNode->type == IntType || tmp->returnExpr->typeNode->type == CharType)
						classFile->push_back(172); //ireturn
					else
						classFile->push_back(176); //areturn
				}
				break;
		}
		tmp = tmp->next;
	}
}

void MethodNode::generation_expr(exprStruct* expr, std::vector<char>* classFile, ClassNode* clss, bool isGet, bool inExpr)
{
	string str;
	int pos;
	switch(expr->type)
	{
		case Appeal:
			generation_appeal(expr->appeal, classFile, clss, isGet);
			break;
		case Nulltoken:
			classFile->push_back(1); //aconst_null
			break;
		case UMinus: //узел только для целочисленных типов
			//умножение на -1
			classFile->push_back(2); //загрузить -1
			generation_expr(expr->left, classFile, clss);
			classFile->push_back(104); //imul - отрицательное число получается умножением на -1
			break;
		case Not:
			// сгенерировать код для выражения
			generation_expr(expr->left, classFile, clss);

			// dup(0x59, 89)
			classFile->push_back(89);

			// если результат - истина, то переход к загрузке 0 
			classFile->push_back(154); // ifne(0x9A, 154)

			// смещение на 0
			writeS2(7, classFile);

			// загружаем 1 и делаем переход
			classFile->push_back(4); // iconst_1

			classFile->push_back(167); // goto
			// записать смещение
			writeS2(4, classFile);

			// загружаем 0
			classFile->push_back(3); // iconst_0			
			break;
		case Plus: //узел сложения только для целочисленных типов
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(96); //iadd			
			break;
		case Minus: //узел вычитания только для целочисленных типов
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(100); //isub			
			break;
		case Mul: //узел только для целочисленных значений
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(104); //imul
			break;
		case Div:
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(108); //idiv
			break;
		case Equal:
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			if(expr->left->typeNode->type == IntType || expr->left->typeNode->type == CharType)
				classFile->push_back(160); //если значения не равны if_icmpne
			else
				classFile->push_back(166); //if_acmpne
			//записать смещение
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//записать смещение
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case NotEqual:
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			if(expr->left->typeNode->type == IntType || expr->left->typeNode->type == CharType)
				classFile->push_back(159); //если значения равны if_icmpeq
			else
				classFile->push_back(165); //if_acmpeq
			//записать смещение
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//записать смещение
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case MoreEqual:
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(161); //если значение меньше if_icmplt
			//записать смещение
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//записать смещение
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case LessEqual:
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(163); //если значение больше if_icmpgt
			//записать смещение
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//записать смещение
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case More:
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(164); //если значение <= if_icmple
			//записать смещение
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//записать смещение
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case Less:
			generation_expr(expr->left, classFile, clss); //загрузить левый операнд на стек
			generation_expr(expr->right, classFile, clss); //загрузить правый операнд на стек
			classFile->push_back(162); //если значение >= if_icmpge
			//записать смещение
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//записать смещение
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case Or:
			// генерируем код для левого операнда
			generation_expr(expr->left, classFile, clss);

			// dup(0x59, 89)
			classFile->push_back(89);

			// если результат - истина, то переход к загрузке истины 
			classFile->push_back(154); // ifne(0x9A, 154)

			pos = classFile->size();

			// генерируем код для правого операнда
			generation_expr(expr->right, classFile, clss);

			// складываем результаты
			classFile->push_back(96); // iadd

			// если сумма равна истине
			classFile->push_back(153); // ifeq(0x99, 153)
			writeS2(7, classFile);

			// загружаем 1 и делаем переход
			classFile->push_back(4); // iconst_1
			classFile->push_back(167); // goto

			// записать смещение
			writeS2(4, classFile);
			// загружаем 0
			classFile->push_back(3); // iconst_0

			// записываем смещение в запомненную ранее позицию
			writeS2(classFile->size()-pos-2, classFile, pos);
			break;
		case And:
			// генерируем код для левого операнда
			generation_expr(expr->left, classFile, clss);

			// dup(0x59, 89)
			classFile->push_back(89);

			// если результат - ложь, то переход к загрузке 0 
			classFile->push_back(153); // ifeq(0x99, 153)

			pos = classFile->size();

			// генерируем код для правого операнда
			generation_expr(expr->right, classFile, clss);

			// перемножаем результаты
			classFile->push_back(104); // imul(0x68, 104)

			// если произведение равно истине
			classFile->push_back(153); // ifeq(0x99, 153)
			writeS2(7, classFile);

			// загружаем 1 и делаем переход
			classFile->push_back(4); // iconst_1
			classFile->push_back(167); // goto

			// записать смещение
			writeS2(4, classFile);

			// загружаем 0
			classFile->push_back(3); // iconst_0

			// записываем смещение в запомненную ранее позицию
			writeS2(classFile->size()-pos+2, classFile, pos);
			break;
		case Assignment:
			generation_expr(expr->right, classFile, clss); //загрузить значение которое надо присвоить
			generation_expr(expr->left, classFile, clss, false); //загрузить значение в переемнную или поле
			if(inExpr) // если присваивание используется как операнд операции, то нужно после присвоения выгрузить значение/ссылку переменной
			{
				if(expr->right->typeNode->type == ObjectType || expr->right->typeNode->type == StringType || expr->right->typeNode->type == AppealType || expr->right->typeNode->isArray == 1)
					classFile->push_back(25); // aload(0x19, 25)
				else
					classFile->push_back(21); // iload(0x15, 21)
				classFile->push_back(expr->left->appeal->varNum);
			}
			break;
		case AssignmentForExprList:
			generation_expr(expr->right, classFile, clss);
			break;
		case New:
			if(expr->newAppeal->isarr == noArr)
			{
				classFile->push_back(187); //new
				writeU2(clss->findClass(expr->typeNode->toString()), classFile); //записать константу класса, объект которого создаем
				classFile->push_back(89); //dup - удвоить ссылку на стеке
			}
			generation_appeal(expr->newAppeal, classFile, clss);

			if(expr->newAppeal->isHaveExprList)
			{
				generation_exprList(expr->newAppeal->exprListOfNew, classFile, clss);
			}
			break;
		case CastOfType:
			generation_expr(expr->right, classFile, clss); //загрузить значение на стек
			if(expr->typeNode->type == IntType) 
			{
				if(expr->right->typeNode->type == ObjectType) //(int)object
				{
					classFile->push_back(192); //checkcast
					writeU2(clss->findClass("java/lang/Integer"), classFile); 

					classFile->push_back(182); //invokevirtual
					writeU2(clss->findMethodref("java/lang/Integer", "intValue", "()I"), classFile);
				}
			}
			else if(expr->typeNode->type == CharType)
			{
				if(expr->right->typeNode->type == ObjectType) //(char)object
				{
					classFile->push_back(192); //checkcast
					writeU2(clss->findClass("java/lang/Character"), classFile); 

					classFile->push_back(182); //invokevirtual
					writeU2(clss->findMethodref("java/lang/Character", "charValue", "()C"), classFile);
				}
				else //(char)int
					classFile->push_back(146); //i2c 
			}
			else if(expr->typeNode->type == ObjectType)
			{
				if(expr->right->typeNode->type == IntType)
				{
					//вызвать статический valueOf
					classFile->push_back(184); //invokestatic
					writeU2(clss->findMethodref("java/lang/Integer","valueOf","(I)Ljava/lang/Integer;"), classFile);
				}
				else if(expr->right->typeNode->type == CharType)
				{
					classFile->push_back(184); //invokestatic
					writeU2(clss->findMethodref("java/lang/Character","valueOf","(C)Ljava/lang/Character;"), classFile);
				}
			}
			else if(expr->typeNode->type == StringType) //к string можно привести только object
			{
				classFile->push_back(192); //checkcast
				writeU2(clss->findClass("java/lang/String"), classFile); 
			}
			else
			{
				classFile->push_back(192); //checkcast
				writeU2(clss->findClass(expr->typeNode->toString()), classFile);
			}
			break;
		case TernaryArray:
			// генерируем код для массива
			generation_appeal(expr->ternaryArrayName, classFile, clss);

			// выполняем выражение для элемента массива и загружаем значени на стек
			generation_expr(expr->ternaryArrayIndex, classFile, clss);

			// генерируем код для записываемого в массив значения
			generation_expr(expr->ternaryExpr, classFile, clss);

			// загружаем значение в массив 
			// для ссылочных типов aastore(0x53, 83)
			if(expr->ternaryExpr->typeNode->type == AppealType || expr->ternaryExpr->typeNode->type == ObjectType || expr->ternaryExpr->typeNode->type == StringType)
				classFile->push_back(83);
			// для численных типов iastore (0x4F, 79) 
			else if(expr->ternaryExpr->typeNode->type == IntType)
				classFile->push_back(79);
			// для типов чар castore (0x55, 85)
			else
				classFile->push_back(85);
			break;
		case TernaryAppeal:
			generation_appeal(expr->ternaryAppeal, classFile, clss); //рассмотреть appeal до поля - положить ссылку на объект на стек
			generation_expr(expr->ternaryExpr, classFile, clss); //положить значение на стек
			generation_appeal(expr->ternaryAppealField, classFile, clss, false); //поле
			break;
	}
}

void MethodNode::generation_appeal(appealStruct* appeal, std::vector<char>* classFile, ClassNode* clss, bool isGet)
{
	switch(appeal->type)
	{
		case MethodAppeal:
			//invokevirtual, invokespecial
			// стек: объект к которому вызывается (если конструктор, то объект уже поместила), аргументы
			// на стек после выполнения: возвращаемое значение, если не void
			if(appeal->method->invoke == 0 || appeal->method->invoke == 1) 
			{
				// 1. Помещение объекта на стек - по идее должо быть уже сделано, надо разобраться с new
				// 2. Поместить аргументы на стек
				generation_factArgList(appeal->method->args, classFile, clss);
				// 3. Выполнить команду
				if(appeal->method->invoke == 0)
					classFile->push_back(182); //invokevirtual
				else
					classFile->push_back(183); //invokespecial
				//для выполнения команды также нужно загрузить methodref
				writeU2(appeal->method->methodref, classFile);
			}
			else
			{
				// 1. Поместить аргументы на стек
				generation_factArgList(appeal->method->args, classFile, clss);

				classFile->push_back(184);//invokestatic
				//для выполнения команды также нужно загрузить methodref
				writeU2(appeal->method->methodref, classFile);
			}
			break;
		case ArrayAppeal:
			// iaload (0x2E, 46) и aaload(0x32, 50), caload (0x34, 52)
			// стек до выполнения: ссылка на массив, индекс элемента
			// стек после: значение элемента массива
			
			// генерируем код для массива
			generation_appeal(appeal->mass, classFile, clss);

			// выполняем выражение для элемента массива и загружаем значени на стек
			generation_expr(appeal->num, classFile, clss);

			// выполняем команду взятия элемента
			// для ссылочных типов aaload
			if(appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == StringType)
				classFile->push_back(50);
			// для чаров сaload
			else if(appeal->typeNode->type == CharType)
				classFile->push_back(52);
			// для чисел iaload
			else
				classFile->push_back(46);
			break;
		case Int:
			// загружаем число на стек
			// bipush (0x10, 16) для однобайтовой
			if(appeal->num_appeal >= -128 && appeal->num_appeal <= 127)
			{
				classFile->push_back(16);
				classFile->push_back(appeal->num_appeal);
			}
			// sipush (0x11, 17) - двухбайтовую
			else if(appeal->num_appeal >= -32768 && appeal->num_appeal <= 32767)
			{
				classFile->push_back(17);
				writeS2(appeal->num_appeal, classFile);
			}
			// lcd (0x12, 18) или ldc_w (0x13, 19) - от трех байт и больше (из таблицы констант)
			else
			{
				if(clss->findInteger(appeal->num_appeal) >= -128 && clss->findInteger(appeal->num_appeal) <= 127)
				{
					classFile->push_back(19);
					classFile->push_back(clss->findInteger(appeal->num_appeal));
				}
				else
				{
					classFile->push_back(19);
					writeU2(clss->findInteger(appeal->num_appeal), classFile);
				}
			}
			break;
		case Charval:
			// всегда 1 байт, используем bipush (0x10, 16)
			classFile->push_back(16);
			classFile->push_back(appeal->character_appeal);
			break;
		case Stringval:
			// используем lcd (0x12, 18) или ldc_w (0x13, 19), указав номер константы из таблицы констант
			if(clss->findString(appeal->string_appeal) >= -128 && clss->findString(appeal->string_appeal) <= 127)
			{
				classFile->push_back(18);
				classFile->push_back(clss->findString(appeal->string_appeal));
			}
			else
			{
				classFile->push_back(19);
				writeU2(clss->findString(appeal->string_appeal), classFile);
			}
			break;
		case True:
			// записываем как 1,  iconst_1: 0x4
			classFile->push_back(4);
			break;
		case False:
			// записываем как 0, используя  iconst_0: 0x3
			classFile->push_back(3);
			break;
		case IdAppeal:
		case GetAppeal:
		case SetAppeal:
			// если это поле
			if(appeal->isField)
			{
				if(isGet)				
					classFile->push_back(180); // используем getfield(0xB4, 180)
				else
					classFile->push_back(181); //putfield
				writeU2(appeal->fieldref, classFile);
				
			}
			// если это локальная переменная
			else
			{
				// если это взятие значения ИЗ переменной
				if(isGet)
				{
					// если это ссылка
					if(appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == StringType || appeal->typeNode->isArray == 1)
					{
						// загружаем на стек ссылку из локальной переменной, используя aload(0x19, 25)
						classFile->push_back(25);
					}
					// если это значение
					else
					{
						// загружаем на стек число из локальной переменной, используя iload(0x15, 21)
						classFile->push_back(21);
					}					
				}
				// если это загрузка значения В переменную
				else
				{
					// если это ссылка
					if(appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == StringType || appeal->typeNode->isArray == 1)
					{
						// загружаем ссылку со стека в локальную переменную, используя astore(0x3A, 58)
						classFile->push_back(58);
					}
					// если это значение
					else
					{
						// загружаем значение со стека в локальную переменную, используя  istore(0x36, 54)
						classFile->push_back(54);
					}
				}
				// загружаем номер локальной переменной
				classFile->push_back(appeal->varNum);
			}
			break;
		case BaseAppeal:
		case ThisAppeal:
			// загружаем на стек this с помощью  aload (0x19, 25)
			classFile->push_back(25);
			classFile->push_back(0);
			break;		
		case NewAppeal:
			classFile->push_back(187); //new
			writeU2(clss->findClass(appeal->typeNode->toString()), classFile); //записать константу класса, объект которого создаем
			classFile->push_back(89); //dup - удвоить ссылку на стеке
			break;
		case NewWithExprAppeal:
			generation_createArray(appeal, classFile, clss);
			break;
		case NewWithExprListAppeal:
			generation_createArray(appeal, classFile, clss);

			// Инициализируем массив начальными значениями
			if(appeal->exprListOfNew != NULL)
			{
				exprListStruct * list = appeal->exprListOfNew;
				int i = 0;
				while(list != NULL)
				{
					// продублируем ссылку на массив в стеке с помощью  dup(0x59, 89)
					classFile->push_back(89);

					// запишем индекс массива в стек
					generation_expr(createAppealExpr(createIntAppeal(i)), classFile, clss);

					// запишем выражение в стек
					generation_expr(list->expr, classFile, clss);

					// если массив ссылочного типа 
					if(appeal->typeNode->type == StringType ||  appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType)
					{
						// используем  aastore (0x53, 83)
						classFile->push_back(83);
					}
					// если массив чаров
					else if(appeal->typeNode->type == CharType)
					{
						// используем castore (0x55, 85)
						classFile->push_back(85);
					}
					// если значимого типа
					else
					{
						// используем  iastore (0x4F, 79)
						classFile->push_back(79);
					}
					i++;
					list = list->next;
				}
			}
			break;
		case Point:
			generation_appeal(appeal->left,classFile, clss);
			generation_appeal(appeal->right, classFile, clss, isGet);
			break;
	}
}

void MethodNode::generation_createArray(appealStruct* appeal, std::vector<char>* classFile, ClassNode* clss)
{
	// загружаем размер на стек
	generation_expr(appeal->exprOfNew, classFile, clss);

	// если массив ссылочных типов
	if(appeal->typeNode->type == StringType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == AppealType)
	{
		// создаем массив с помощью команды anewarray (0xBD, 189)
		classFile->push_back(189);

		// записываем тип элементов массива, взяв его из таблицы констант
		writeU2(clss->findClass(appeal->typeOfNew->toStringForDescriptor()), classFile);
	}
	// если массив значимых типов
	else
	{
		// создаем массив с помощью команды newarray (0xBC, 188)
		classFile->push_back(188);

		// записываем тип элементов массива
		if(appeal->typeOfNew->type == IntType)
			classFile->push_back(10); // T_INT 10 
		else if(appeal->typeOfNew->type == CharType)
			classFile->push_back(5); //T_CHAR 5
	}
}

void MethodNode::generation_factArgList(factArgListStruct* factArgs, std::vector<char>* classFile, ClassNode* clss)
{
	while(factArgs != NULL)
	{
		generation_expr(factArgs->expr, classFile, clss);
		factArgs = factArgs->next;
	}
}

void MethodNode::generation_ifStmt(ifElseStruct * ifElse, std::vector<char>* classFile, ClassNode* clss)
{	
	ifElseStruct * tmp = ifElse;
	stmtStruct * elseStmt = NULL;

	// создаем вектор векторов
	vector <vector<char>> codeOfBranches;
	vector <vector<char>> codeOfConditions;
	vector<char> tmpCode;
	vector<char> tmpCondition;
	while(tmp != NULL)
	{
		// проходим по всем веткам и для каждой генерируем код
		if(tmp->body != NULL)
			generation_stmtList(tmp->body->stmtList, &tmpCode, clss);
		generation_expr(tmp->condition, &tmpCondition, clss);

		codeOfBranches.push_back(tmpCode);
		codeOfConditions.push_back(tmpCondition);
		tmpCode.clear();
		tmpCondition.clear();

		if(tmp->nextBranch != NULL)
		{
			if(tmp->nextBranch->type == IfStmt)
				tmp = tmp->nextBranch->ifElse;
			else
			{
				elseStmt = tmp->nextBranch;
				break;
			}
		}
		else
			break;
	}
	if(elseStmt != NULL)
	{
		generation_stmtList(elseStmt->stmtList, &tmpCode, clss);
		codeOfBranches.push_back(tmpCode);
	}
	int count = codeOfBranches.size();

	int stepToEnd = 0;

	for(int i = count-1; i >= 0; i--)
	{
		if(elseStmt == NULL || i != codeOfBranches.size()-1)
		{
			// в конец каждой ветки вставляем безусловный переход на КОНЕЦ РАЗВИЛКИ (кроме последней ветки) командой goto(0xA7, 167)
			if(i+1 != codeOfBranches.size())
			{
				codeOfBranches[i].push_back(167);
				writeS2(stepToEnd, &codeOfBranches[i]);
				stepToEnd += 1;
			}

			// в начало каждой ветки вставляем код для условия и переход на НАЧАЛО СЛЕДУЮЩЕЙ ВЕТКИ, если условие не истинно (кроме else) командой ifeq(0x99, 153)
			codeOfConditions[i].push_back(153);
			writeS2(codeOfBranches[i].size()+3, &codeOfConditions[i]);
			stepToEnd += 3;		
		
			// Добавляем к коду условия код тела ветки
			codeOfConditions[i].insert(codeOfConditions[i].end(), codeOfBranches[i].begin(), codeOfBranches[i].end());
		}

		if(elseStmt != NULL)
			stepToEnd += codeOfBranches[i].size()+2;
		if(elseStmt == NULL)
			stepToEnd += codeOfConditions[i].size();
	}

	// проходим по всем векторам и добавляем код каждой ветки к исходному коду
	for(int i = 0; i < codeOfConditions.size(); i++)
	{
		classFile->insert(classFile->end(), codeOfConditions[i].begin(), codeOfConditions[i].end());
	}
	if(elseStmt != NULL)
		classFile->insert(classFile->end(), codeOfBranches[codeOfBranches.size()-1].begin(), codeOfBranches[codeOfBranches.size()-1].end());
}

void MethodNode::generation_whileStmt(whileStruct * loop, std::vector<char>* classFile, ClassNode* clss)
{
	vector <char> bodyCode;
	vector <char> conditionCode;
	// генерируем код для условия
	if(!loop->fromForeach)
	{
		generation_expr(loop->condition, &conditionCode, clss);
	}
	else
	{
		// тут короче вместо правого элемента написать взятие размера массива ($counter < $arr.length())
		generation_expr(loop->condition->left, &conditionCode, clss); //загрузить левый операнд на стек
		
		// загружаем на стек ссылку из локальной переменной, используя aload(0x19, 25)
		conditionCode.push_back(25);
		// загружаем номер локальной переменной
		conditionCode.push_back(loop->condition->right->appeal->varNum);
		// arraylength (0xBE, 190)
		conditionCode.push_back(190);

		conditionCode.push_back(162); //если значение >= if_icmpge
		//записать смещение
		writeS2(7, &conditionCode);
		conditionCode.push_back(4); //iconst_1
		conditionCode.push_back(167); //goto
		//записать смещение
		writeS2(4, &conditionCode);
		conditionCode.push_back(3); //iconst_0
	}

	// генерируем код для тела
	generation_stmtList(loop->body->stmtList, &bodyCode, clss);

	// генерируем проверку истинности условия и переход за пределы цикла ifeq(0x99, 153)
	conditionCode.push_back(153);
	writeS2(bodyCode.size()+6, &conditionCode);

	// в конец тела цикла записываем безусловный переход на условие цикла с помощью goto(0xA7, 167)
	bodyCode.push_back(167);
	writeS2(-bodyCode.size()-conditionCode.size(), &bodyCode);

	// копируем в конец условие и тело цикла
	classFile->insert(classFile->end(), conditionCode.begin(), conditionCode.end());
	classFile->insert(classFile->end(), bodyCode.begin(), bodyCode.end());
}

void MethodNode::generation_doWhileStmt(whileStruct * loop, std::vector<char>* classFile, ClassNode* clss)
{
	vector <char> bodyCode;

	// генерируем код для тела цикла
	generation_stmtList(loop->body->stmtList, &bodyCode, clss);

	// генерируем код для условия цикла
	generation_expr(loop->condition, &bodyCode, clss);

	// в конец условия вставляем проверку истинности и переход на начало цикла ifne(0x9A, 154)
	bodyCode.push_back(154);
	writeS2(-bodyCode.size(), &bodyCode);

	// копируем код в класс файл
	classFile->insert(classFile->end(), bodyCode.begin(), bodyCode.end());
}

void MethodNode::generation_exprList(exprListStruct* exprList, std::vector<char>* classFile, ClassNode* clss)
{
	exprListStruct* tmp = exprList;
	int count = 0;
	while(tmp != NULL)
	{
		//увеличить количество ссылок в стеке

		tmp = tmp->next;
	}
}

void MethodNode::generation_exprForList(exprListStruct* exprList, std::vector<char>* classFile, ClassNode* clss)
{

}